#include <iostream> 
#include <thread> 
#include "SynchroQueue.hpp"
using namespace std; 
 
// g++ -o  main.cpp -lpthread      g++ *.cpp -I. -lpthread -o main
Synchroqueue<int> Q;
void Prod(){
	for (int i=100;i>0;--i){
		Q.put(i);
	}
	std::cout << "Production Process Finished" <<endl; 
}

int main() {
	std::thread t_Prod(Prod);
	for (int i=0;i<100;++i){
	std::cout << "ELEMENT "<<i<<" : "<< Q.get() <<endl;
	}
	 t_Prod.join();
return 0;
}
