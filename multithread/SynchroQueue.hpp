#ifndef SYNCHROQUEUE_HPP
#define SYNCHROQUEUE_HPP

#include <mutex>
#include <list>
#include <condition_variable>
template <class T>
class Synchroqueue{
private:
	std::mutex m_mutex;
	std::condition_variable myCv;
	std::list<T> m_list;
public:
	//Synchroqueue(int in_sizeqeueux);
	void put(const T&);
	T get();
};

template <typename T> 
void Synchroqueue<T>::put(const T& in_rrElement){
	std::unique_lock<std::mutex> lck(m_mutex);
	m_list.push_back(in_rrElement);
	myCv.notify_one();
}


template <typename T>
T Synchroqueue<T>::get(){
	std::unique_lock<std::mutex> lck(m_mutex);
	while (m_list.empty()){
		myCv.wait(lck);
	}
	T l_ElementTmp = m_list.front();
	m_list.pop_front();
	return l_ElementTmp;	
}




#endif //SYNCHROQUEUE_HPP  
