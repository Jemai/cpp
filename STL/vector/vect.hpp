#ifndef VECT_HPP
#define VECT_HPP

#include <exception>

template<class T>
class vect{
private:
	T* p_array;
	int m_size;
	int m_capacity;
	class iterator;
public:
	vect();
	vect(int in_size);
	int size();
	T& operator[](int);
        void push_back(const T&);
	template<typename... Args>
        void emplace_back(Args&&...);
	iterator begin();
	iterator end();
};


template<class T>
class vect<T>::iterator{
vect<T>& rVect;
int pos_it;
public:
	iterator(int pos, vect<T>& in_rvect);
	T& operator*();
	iterator& operator++();
	bool operator<(const iterator&);
};
template<class T>
bool vect<T>::iterator::operator<(const vect<T>::iterator& in_rit){
	return (pos_it<(in_rit.pos_it));
}

template<typename T>
typename vect<T>::iterator& vect<T>::iterator::operator++(){
	++pos_it;
	return *this; 
}

template<typename T>
vect<T>::iterator::iterator(int pos,vect<T>& in_rvect):pos_it(pos),rVect(in_rvect){

}

template<typename T>
T& vect<T>::iterator::operator*(){
	return rVect[pos_it];
}


template<typename T>
typename vect<T>::iterator vect<T>::begin(){
	return iterator(0,*this);
}

template<typename T>
typename vect<T>::iterator vect<T>::end(){
	return iterator(m_size,*this);
}


template<typename T>
void vect<T>::push_back(const T& in_val){
	if(m_size >= m_capacity){
		m_capacity *=2; 
		T* p_array_tmp = new T[m_capacity];
		for(int i=0;i<m_size;++i){
			p_array_tmp[i]=p_array[i];
		}
		delete[] p_array;
		p_array=p_array_tmp;
		p_array_tmp=nullptr;
	}
	else{
	}
	p_array[m_size++] = in_val;
}


template<typename T>template <typename... Args>
void vect<T>::emplace_back(Args&&... args){
	if(m_size >= m_capacity){
		m_capacity *=2; 
		T* p_array_tmp = new T[m_capacity];
		for(int i=0;i<m_size;++i){
			p_array_tmp[i]=p_array[i];
		}
		delete[] p_array;
		p_array=p_array_tmp;
		p_array_tmp=nullptr;
	}
	else{
	}
	p_array[m_size++] = T(std::forward<Args>(args)...);
}

template<typename T>
T& vect<T>::operator[](int in_index){
	if(in_index<m_size){
		
	}
        else{
		throw "wrong index";	
	}
	return p_array[in_index];
}





// Definition Part
template<class T>
vect<T>::vect(){
	m_size = 0;
	m_capacity = 10;
	p_array = new T[m_capacity];
}

template<class T>
vect<T>::vect(int in_size){
	m_size = in_size;
	m_capacity = m_size;
	p_array = new T[m_capacity];
}
template<class T>
int vect<T>::size(){
return m_size;
}



#endif //VECT_HPP
