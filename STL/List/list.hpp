#ifndef LIST_HPP
#define LIST_HPP

template <class T> class Link;
template <class T> class List_iterator;

template <class T> 
class List
{
protected:
	Link<T>* m_plinkHead{nullptr};
	Link<T>* m_plinkTail{nullptr};
	int m_isize{0};
public:
	typedef List_iterator<T> iterator;
	List(const List<T> & l);
	~List();
	bool empty() const;
	unsigned int size() const; 
	T & back() const;
	T & front() const;
	void push_front(const T & x);
	void push_back(const T & x);
	void pop_front();
	void pop_back();
	iterator begin() const;
	iterator end() const;
	void insert(iterator pos, const T & x);
	void erase(iterator & pos); 
	List<T> & operator=(const List<T> & l);
};

template <class T> 
class Link 
{
private:
   Link(const T & x): value(x), next_link(nullptr), prev_link(nullptr) {}//pg. 204 

   T value;     
   Link<T> * next_link{nullptr};
   Link<T> * prev_link{nullptr};

   friend class List<T>;
   friend class List_iterator<T>;
};
	
template <class T> class List_iterator
{
public:
   typedef List_iterator<T> iterator;

   List_iterator(Link<T> * source_link): current_link(source_link) { }
   List_iterator(): current_link(0) { }
   List_iterator(List_iterator<T> * source_iterator): current_link(source_iterator.current_link) { }

   T & operator*();  // dereferencing operator
   iterator & operator=(const iterator & rhs);
   bool operator==(const iterator & rhs) const;
   bool operator!=(const iterator & rhs) const;
   iterator & operator++(); 
   iterator operator++(int);
   iterator & operator--(); 
   iterator operator--(int); 

protected:
   Link<T> * current_link;

   friend class List<T>;
};


#endif //LIST_HPP
