#include <stdio.h>
#include <stdlib.h>

struct Array{
	int* A;
	int size;
	int length;
};

void Display(struct Array in_A){
	int i=0;
	for(i=0;i<in_A.length;++i){
		printf("A[%d] = %d \n",i,in_A.A[i]);
	}
}
int main(){
	struct Array st_Array;
	int i=0,n;
	printf("Enter the size of Array\n");
	scanf("%d",&st_Array.size);
	st_Array.A = (int*)malloc(sizeof(int)* st_Array.size);
	st_Array.length = 0;
	printf("Enter the length of Array\n");
	scanf("%d",&n);
	for(i=0;i<n;++i){
		printf("Enter the %d Element\n",i);
		scanf("%d",&st_Array.A[i]);
		++st_Array.length;
	}
	Display(st_Array);
	/*Desallocate the memory*/
	free(st_Array.A);
	return 0;
}
